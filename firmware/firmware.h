//
// Created by matthias on 14.08.19.
//

#ifndef BURNER_WORDCLOCK_FIRMWARE_H
#define BURNER_WORDCLOCK_FIRMWARE_H

#include <FastLED.h>

void fastTest();

void clockLogic();

void doLDRLogic();

void makeParty();

void off();

void showHeart();

void pushToStrip(int ledId);

void resetAndBlack();

void resetStrip();

void displayStripRandomColor();

void displayStrip();

void displayStrip(CRGB colorCode);

void timeToStrip(uint8_t hours, uint8_t minutes);

void doDCFLogic();

void handleRTCInterrupt();

void displayNumber(uint8_t i);

CRGB getColorForIndex();

void display00();

void display10();

void display20();

void display30();

void display40();

void display50();

void display0();

void display1();

void display2();

void display3();

void display4();

void display5();

void display6();

void display7();

void display8();

void display9();

void pushES_IST();

void pushFUENF1();

void pushNACH();

void pushZEHN1();

void pushVIERTEL();

void pushZWANZIG();

void pushVOR();

void pushHALB();

void pushDREIVIERTEL();

void pushZWOELF();

void pushEINS(bool mit_s);

void pushZWEI();

void pushDREI();

void pushVIER();

void pushFUENF2();

void pushSECHS();

void pushSIEBEN();

void pushACHT();

void pushNEUN();

void pushZEHN();

void pushELF();

void pushUHR();

#endif //BURNER_WORDCLOCK_FIRMWARE_H
