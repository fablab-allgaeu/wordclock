/*

(c) 2014 - Markus Backes - https://backes-markus.de/blog/
(c) 2019 - Matthias Köferlein - https://gitlab.com/fablab-allgaeu/wordclock

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Dieses Programm ist Freie Software: Sie können es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
veröffentlichten Version, weiterverbreiten und/oder modifizieren.

Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber
OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
Siehe die GNU General Public License für weitere Details.

Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
*/

//#define DEBUG

#include "firmware.h"

//Library includes
#include <FastLED.h>
#include <Wire.h>
#include <TimeLib.h>
#include <DS3232RTC.h>
#include "default_layout.h" //#include "alt_layout1.h"

//LED defines
#define NUM_LEDS 110

//PIN defines
#define STRIP_DATA_PIN 6
#define ARDUINO_LED 13 //Default Arduino LED
#define LDR_PIN 0
#define RTC_1HZ_PIN 2
#define MENU_PIN 3
#define PLUS_PIN 4

uint8_t strip[NUM_LEDS];
uint8_t stackptr = 0;

enum MenuState {
    TIME_DISPLAY,  // ganz normal die Zeit anzeigen
    INIT_MENU, // Menü initialisieren (Variablen setzen, Stunden anzeigen...)
    SET_HOURS, // Stunden einstellen
    SET_MINUTES, // Minuten einstellen
    WRITE_TIME_TO_RTC, // eingestellte Zeit in die RTC übernehmen
    RESUME_TIME_DISPLAY // zum Normalbetrieb zurückkehren
};

CRGB leds[NUM_LEDS];
// Variablen für das Stellen der Uhrzeit
static const uint16_t MENU_TIMEOUT_MS = 10000; // man hat 10 Sekunden Zeit, bevor die Uhr wieder die Uhrzeit anzeigt
static const uint16_t DEBOUNCE_TIME_MS = 50; // Taster können prellen, deswegen warten wir immer 50ms, bevor wir einen neuen Tastendruck registrieren
uint16_t lastPress = 0; // speichert die Zeit, zu der das letzte Mal ein Knopf gedrückt wurde

MenuState menuState = TIME_DISPLAY;

bool plusDown = false; // ob der Plus Knopf gedrückt gehalten wird
uint16_t plusDebounce = 0; // verhindert das Prellen des Knopfes
bool menuDown = false;
uint16_t menuDebounce = 0;

uint8_t settingHour = 0; // hier werden während des Stellens die Stunden gespeichert, bis sie in die RTC geschrieben werden
uint8_t settingMinute = 0;

CRGB defaultColor = CRGB::White;
uint8_t colorIndex = 0;

uint8_t testHours = 0;
uint8_t testMinutes = 0;

#ifdef DEBUG
#define DEBUG_PRINT(...) Serial.println(x, __VA_ARGS__)
#else
#define DEBUG_PRINT(...)
#endif

void setup() {

#ifdef DEBUG
    Serial.begin(9600);
#endif

    pinMode(MENU_PIN, INPUT_PULLUP);
    pinMode(PLUS_PIN, INPUT_PULLUP);

    pinMode(RTC_1HZ_PIN, INPUT_PULLUP); // Pullup am RTC Interrupt Pin setzen, weil der open drain ist
    // Ein Interrupt ist eine Funktion von Mikrocontrolern, die es uns erlaube den Programmfluss zu unterbrechen
    // und auf dringende äußere Signale zu reagieren. Das RTC Modul hat die Möglichkeit ein 1 Hz Rechtecksignal
    // zu erzeugen, sodass wie uns jede Sekunde einmal benachrichtigen lassen können, dass eine Sekunde vergangen ist.
    // Mit diesem Aufruf verbinden wir das 1 Hz Signal an Pin RTC_1HZ_PIN mit der "handleRTCInterrupt" Funktion, einer
    // sogenannten ISR, Interrupt Service Routine
    attachInterrupt(digitalPinToInterrupt(RTC_1HZ_PIN), handleRTCInterrupt, FALLING);
    RTC.squareWave(SQWAVE_1_HZ); // Wir müssen das 1 Hz Signal am RTC Modul auch noch aktivieren
    setSyncProvider(RTC.get);

    pinMode(ARDUINO_LED, OUTPUT);

    // FastLED Bibliothek initialisieren
    for (int i = 0; i < NUM_LEDS; i++) {
        strip[i] = 0;
    }
    FastLED.addLeds<WS2812B, STRIP_DATA_PIN, GRB>(leds, NUM_LEDS);
    resetAndBlack();
    displayStrip();
}

void loop() {
    // erkennt den ersten Tastendruck auf Menu
    if (millis() >= menuDebounce + DEBOUNCE_TIME_MS && menuState == TIME_DISPLAY) {
        if (digitalRead(MENU_PIN) == HIGH) {
            if (!menuDown) {
                lastPress = millis();
                menuDebounce = millis();
                menuDown = true;
                menuState = INIT_MENU;
            }
        } else {
            if (menuDown) {
                menuDebounce = millis();
            }
            menuDown = false;
        }
    }

    if (menuState != TIME_DISPLAY) {
        if (millis() >= lastPress +
                        MENU_TIMEOUT_MS) { // wir prüfen, ob der Timeout abgelaufen ist (der Nutzer hat x Sekunden keine Taste mehr gedrückt)
            menuState = RESUME_TIME_DISPLAY;
            DEBUG_PRINT("Timeout");
        }
        if (menuState == INIT_MENU) { // Variablen initialisieren
            settingHour = hour();
            settingMinute = minute();
            resetAndBlack();
            displayNumber(settingHour);
            displayStrip();
            menuState = SET_HOURS;
            DEBUG_PRINT("Setting clock");
        } else if (menuState == SET_HOURS || menuState == SET_MINUTES) { // Zeit stellen

            // wir möchten mit dem Menu Knopf Minuten -> Stunden -> Bestätigen weiterschalten können
            if (millis() >= menuDebounce + DEBOUNCE_TIME_MS) {
                if (digitalRead(MENU_PIN) == HIGH) {
                    if (!menuDown) {
                        menuDebounce = millis();
                        DEBUG_PRINT("Now setting: ");
                        if (menuState == SET_HOURS) {
                            DEBUG_PRINT("Hours");
                            menuState = SET_MINUTES;
                        } else if (menuState == SET_MINUTES) {
                            DEBUG_PRINT("Minutes");
                            menuState = WRITE_TIME_TO_RTC;
                        }
                    }
                    menuDown = true;
                } else {
                    if (menuDown) {
                        menuDebounce = millis();
                    }
                    menuDown = false;
                }
            }

            // mit dem Plus Knopf sollen wir die aktuelle Stelle einstellen können
            if (millis() >= plusDebounce + DEBOUNCE_TIME_MS) {
                if (digitalRead(PLUS_PIN) == HIGH) {
                    if (plusDown) {
                        // plus wird gedrückt gehalten
                    } else {
                        // Plus wurde gerade erst gedrückt
                        plusDebounce = millis();
                        DEBUG_PRINT("Press!");
                        if (menuState == SET_HOURS) { // Stunden stellen
                            settingHour++;
                            if (settingHour >= 24) {
                                settingHour = 0;
                            }
                            DEBUG_PRINT("Hours: %i", settingHour);
                        } else if (menuState == SET_MINUTES) { // Minuten einstellen
                            settingMinute++;
                            if (settingMinute >= 60) {
                                settingMinute = 0;
                            }
                            DEBUG_PRINT("Minutes: %i", settingMinute);
                        }
                    }
                    lastPress = millis();
                    plusDown = true;
                } else {
                    if (plusDown) {
                        // Plus wurde gerade losgelassen
                        plusDebounce = millis();
                    }
                    plusDown = false;
                }
            }

            resetAndBlack();
            if (menuState == SET_HOURS) { // wenn wir die Stunden einstellen, wollen wir auch die Stunden anzeigen
                displayNumber(settingHour);
            } else if (menuState == SET_MINUTES) { // ansonsten die Minuten
                displayNumber(settingMinute);
            }
            displayStrip(defaultColor);
        } else if (menuState == WRITE_TIME_TO_RTC) {
            // 4 bedeutet, dass der Nutzer die Zeit erfolgreich eingestellt hat und wir sie nun in die RTC schrieben können
            // Wir brauchen dieses Zustand, damit wir zwischen "Der Nutzer hat die Uhrzeit bestätigt" und "Es gab einen
            // Timeout beim Stellen der Uhrzeit" unterscheiden können.
            tmElements_t tm;
            tm.Hour = settingHour;
            tm.Minute = settingMinute;
            time_t t = makeTime(tm);
            RTC.set(t);
            setTime(t);
            menuState = RESUME_TIME_DISPLAY;
        } else { // > 4, wir sind fertig mit dem Stellen oder hatten einen Timeout, deswegen wieder alles auf Anfang
            menuState = TIME_DISPLAY;
            clockLogic();
        }
    }
}

void handleRTCInterrupt() {
    if (menuState != TIME_DISPLAY) {
        DEBUG_PRINT("No clock logic, setting clock");
        return;
    }

    doLDRLogic();
    clockLogic();
}

void doLDRLogic() {
    int ldrVal = map(analogRead(LDR_PIN), 0, 1023, 0, 250);
    FastLED.setBrightness(255 - ldrVal);
    FastLED.show();
    DEBUG_PRINT("LDR val: %i", ldrVal);
}
///////////////////////
// DISPLAY MODES
///////////////////////

void clockLogic() {
    DEBUG_PRINT("doing clock logic");
    resetAndBlack();
    timeToStrip(hour(), minute());
    displayStrip(defaultColor);
}

void off() {
    DEBUG_PRINT("switching off");
    resetAndBlack();
    displayStrip(CRGB::Black);
}

void makeParty() {
    DEBUG_PRINT("YEAH party party");
    resetAndBlack();
    for (int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CHSV(random(0, 255), 255, 255);
    }
    FastLED.show();
}

void showHeart() {
    DEBUG_PRINT("showing heart");
    resetAndBlack();
    pushToStrip(L29);
    pushToStrip(L30);
    pushToStrip(L70);
    pushToStrip(L89);
    pushToStrip(L11);
    pushToStrip(L48);
    pushToStrip(L68);
    pushToStrip(L91);
    pushToStrip(L7);
    pushToStrip(L52);
    pushToStrip(L107);
    pushToStrip(L6);
    pushToStrip(L106);
    pushToStrip(L5);
    pushToStrip(L105);
    pushToStrip(L15);
    pushToStrip(L95);
    pushToStrip(L23);
    pushToStrip(L83);
    pushToStrip(L37);
    pushToStrip(L77);
    pushToStrip(L41);
    pushToStrip(L61);
    pushToStrip(L59);
    displayStrip(CRGB::Red);
}

void fastTest() {
    if (testMinutes >= 60) {
        testMinutes = 0;
        testHours++;
    }
    if (testHours >= 24) {
        testHours = 0;
    }

    //Array leeren
    resetAndBlack();
    timeToStrip(testHours, testMinutes);
    displayStrip(defaultColor);
    testMinutes++;
}

///////////////////////

CRGB prevColor() {
    if (colorIndex > 0) {
        colorIndex--;
    }
    return getColorForIndex();
}

CRGB nextColor() {
    if (colorIndex < 9) {
        colorIndex++;
    }
    return getColorForIndex();
}

CRGB getColorForIndex() {
    switch (colorIndex) {
        case 0:
            return CRGB::White;
        case 1:
            return CRGB::Blue;
        case 2:
            return CRGB::Aqua;
        case 3:
            return CRGB::Green;
        case 4:
            return CRGB::Lime;
        case 5:
            return CRGB::Red;
        case 6:
            return CRGB::Magenta;
        case 7:
            return CRGB::Olive;
        case 8:
            return CRGB::Yellow;
        case 9:
            return CRGB::Silver;
        default:
            colorIndex = 0;
            return CRGB::White;
    }
}

void pushToStrip(int ledId) {
    strip[stackptr] = ledId;
    stackptr++;
}

void resetAndBlack() {
    resetStrip();
    for (int i = 0; i < NUM_LEDS; i++) {
        leds[i] = CRGB::Black;
    }
}

void resetStrip() {
    stackptr = 0;
    for (int i = 0; i < NUM_LEDS; i++) {
        strip[i] = 0;
    }
}

void displayStripRandomColor() {
    for (int i = 0; i < stackptr; i++) {
        leds[strip[i]] = CHSV(random(0, 255), 255, 255);
    }
    FastLED.show();
}

void displayStrip() {
    displayStrip(defaultColor);
}

void displayStrip(CRGB colorCode) {
    for (int i = 0; i < stackptr; i++) {
        leds[strip[i]] = colorCode;
    }
    FastLED.show();
}

// Ultra-Ranz-Funktion 3000 Plus
void displayNumber(uint8_t i) {
    switch (i / 10) {
        case 0:
            display00();
            break;
        case 1:
            display10();
            break;
        case 2:
            display20();
            break;
        case 3:
            display30();
            break;
        case 4:
            display40();
            break;
        case 5:
            display50();
            break;
        default:
            break;
    }
    switch (i % 10) {
        case 0:
            display0();
            break;
        case 1:
            display1();
            break;
        case 2:
            display2();
            break;
        case 3:
            display3();
            break;
        case 4:
            display4();
            break;
        case 5:
            display5();
            break;
        case 6:
            display6();
            break;
        case 7:
            display7();
            break;
        case 8:
            display8();
            break;
        case 9:
            display9();
            break;
    }
}

void timeToStrip(uint8_t hours, uint8_t minutes) {
    pushES_IST();

    if (hours >= 12) {
        hours -= 12;
    }

    // weil sich alles ab "fünf vor halb x" auf die nächste Stunde bezieht
    if (minutes >= 25) {
        hours++;
    }

    // weil sich "viertel x" auf die nächste Stunde bezieht
    if (minutes >= 15 && minutes < 20) {
        hours++;
    }

    if (hours == 12) {
        hours = 0;
    }

    //show minutes
    if (minutes >= 5 && minutes < 10) {
        pushFUENF1();
        pushNACH();
    } else if (minutes >= 10 && minutes < 15) {
        pushZEHN1();
        pushNACH();
    } else if (minutes >= 15 && minutes < 20) {
        pushVIERTEL();
    } else if (minutes >= 20 && minutes < 25) {
        pushZWANZIG();
        pushNACH();
    } else if (minutes >= 25 && minutes < 30) {
        pushFUENF1();
        pushVOR();
        pushHALB();
    } else if (minutes >= 30 && minutes < 35) {
        pushHALB();
    } else if (minutes >= 35 && minutes < 40) {
        pushFUENF1();
        pushNACH();
        pushHALB();
    } else if (minutes >= 40 && minutes < 45) {
        pushZWANZIG();
        pushVOR();
    } else if (minutes >= 45 && minutes < 50) {
        pushDREIVIERTEL();
    } else if (minutes >= 50 && minutes < 55) {
        pushZEHN1();
        pushVOR();
    } else if (minutes >= 55 && minutes < 60) {
        pushFUENF1();
        pushVOR();
    }

    //show hours
    switch (hours) {
        case 0:
            pushZWOELF();
            break;
        case 1:
            // Sonderfall, da es "Ein Uhr" ist, aber "Fünf nach Eins", also brauchen wir ab 5 Minuten ein 's'
            pushEINS(minutes > 4);
            break;
        case 2:
            pushZWEI();
            break;
        case 3:
            pushDREI();
            break;
        case 4:
            pushVIER();
            break;
        case 5:
            pushFUENF2();
            break;
        case 6:
            pushSECHS();
            break;
        case 7:
            pushSIEBEN();
            break;
        case 8:
            pushACHT();
            break;
        case 9:
            pushNEUN();
            break;
        case 10:
            pushZEHN();
            break;
        case 11:
            pushELF();
            break;
    }

    // wenn es genau X Uhr ist, können wir das Wort "UHR" unten anzeigen lassen
    if (minutes < 5) {
        pushUHR();
    }
}

///////////////////////
// PUSH WORD HELPER
//
// Diese Funktionen setzen die LEDs, die wir für jedes Wort brauchen
///////////////////////
void pushES_IST() {
    pushToStrip(L9);
    pushToStrip(L10);
    pushToStrip(L30);
    pushToStrip(L49);
    pushToStrip(L50);
}

void pushFUENF1() {
    pushToStrip(L70);
    pushToStrip(L89);
    pushToStrip(L90);
    pushToStrip(L109);
}

void pushFUENF2() {
    pushToStrip(L76);
    pushToStrip(L83);
    pushToStrip(L96);
    pushToStrip(L103);
}

void pushNACH() {
    pushToStrip(L26);
    pushToStrip(L33);
    pushToStrip(L46);
    pushToStrip(L53);
}

void pushZEHN1() {
    pushToStrip(L8);
    pushToStrip(L11);
    pushToStrip(L28);
    pushToStrip(L31);
}

void pushVIERTEL() {
    pushToStrip(L47);
    pushToStrip(L52);
    pushToStrip(L67);
    pushToStrip(L72);
    pushToStrip(L87);
    pushToStrip(L92);
    pushToStrip(L107);
}

void pushDREIVIERTEL() {
    pushToStrip(L7);
    pushToStrip(L12);
    pushToStrip(L27);
    pushToStrip(L32);
    pushToStrip(L47);
    pushToStrip(L52);
    pushToStrip(L67);
    pushToStrip(L72);
    pushToStrip(L87);
    pushToStrip(L92);
    pushToStrip(L107);
}

void pushVOR() {
    pushToStrip(L66);
    pushToStrip(L73);
    pushToStrip(L86);
}

void pushHALB() {
    pushToStrip(L5);
    pushToStrip(L14);
    pushToStrip(L25);
    pushToStrip(L34);
}

void pushZWANZIG() {
    pushToStrip(L48);
    pushToStrip(L51);
    pushToStrip(L68);
    pushToStrip(L71);
    pushToStrip(L88);
    pushToStrip(L91);
    pushToStrip(L108);
}

void pushZWOELF() {
    pushToStrip(L54);
    pushToStrip(L65);
    pushToStrip(L74);
    pushToStrip(L85);
    pushToStrip(L94);
}

void pushEINS(bool s) {
    pushToStrip(L24);
    pushToStrip(L35);
    pushToStrip(L44);
    if (s) {
        pushToStrip(L55);
    }
}

void pushZWEI() {
    pushToStrip(L4);
    pushToStrip(L15);
    pushToStrip(L24);
    pushToStrip(L35);
}

void pushDREI() {
    pushToStrip(L16);
    pushToStrip(L23);
    pushToStrip(L36);
    pushToStrip(L43);
}

void pushVIER() {
    pushToStrip(L77);
    pushToStrip(L82);
    pushToStrip(L97);
    pushToStrip(L102);
}

void pushSECHS() {
    pushToStrip(L19);
    pushToStrip(L20);
    pushToStrip(L39);
    pushToStrip(L40);
    pushToStrip(L59);
}

void pushSIEBEN() {
    pushToStrip(L55);
    pushToStrip(L64);
    pushToStrip(L75);
    pushToStrip(L84);
    pushToStrip(L95);
    pushToStrip(L104);
}

void pushACHT() {
    pushToStrip(L18);
    pushToStrip(L21);
    pushToStrip(L38);
    pushToStrip(L41);
}

void pushNEUN() {
    pushToStrip(L37);
    pushToStrip(L42);
    pushToStrip(L57);
    pushToStrip(L62);
}

void pushZEHN() {
    pushToStrip(L58);
    pushToStrip(L61);
    pushToStrip(L78);
    pushToStrip(L81);
}

void pushELF() {
    pushToStrip(L2);
    pushToStrip(L17);
    pushToStrip(L22);
}

void pushUHR() {
    pushToStrip(L80);
    pushToStrip(L99);
    pushToStrip(L100);
}

///////////////////////
// Diese Funktionen zeigen große Ziffern auf der Uhr an. Sie werden beim Stellen der Uhr genutzt,
// damit wir sie Minutengenau einstellen können, statt nur auf 5 Minuten genau mit den Wörtern.
// Wir können auch nur Zehnerstellen bis 5X anzeigen, weil ich faul bin und die höchste Zahl,
// die wir brauchen, 59 ist.
///////////////////////

void display00() {
                    pushToStrip(L11); pushToStrip(L28); pushToStrip(L31);
  pushToStrip( L7);                                                       pushToStrip(L47);
  pushToStrip( L6);                                                       pushToStrip(L46);
  pushToStrip( L5);                                                       pushToStrip(L45);
  pushToStrip( L4);                                                       pushToStrip(L44);
  pushToStrip( L3);                                                       pushToStrip(L43);
  pushToStrip( L2);                                                       pushToStrip(L42);
                    pushToStrip(L18); pushToStrip(L21); pushToStrip(L38);
}

void display10() {
                                      pushToStrip(L28);
                    pushToStrip(L12); pushToStrip(L27);
  pushToStrip( L6);                   pushToStrip(L26);
                                      pushToStrip(L25);
                                      pushToStrip(L24);
                                      pushToStrip(L23);
                                      pushToStrip(L22);
  pushToStrip( L1); pushToStrip(L18); pushToStrip(L21); pushToStrip(L38); pushToStrip(L41);
}

void display20() {
                    pushToStrip(L11); pushToStrip(L28); pushToStrip(L31);
  pushToStrip( L7);                                                       pushToStrip(L47);
                                                                          pushToStrip(L46);
                                                                          pushToStrip(L45);
                                                        pushToStrip(L35);
                                      pushToStrip(L23);
                    pushToStrip(L17);
  pushToStrip( L1); pushToStrip(L18); pushToStrip(L21); pushToStrip(L38); pushToStrip(L41);
}

void display30() {
                    pushToStrip(L11); pushToStrip(L28); pushToStrip(L31);
  pushToStrip( L7);                                                       pushToStrip(L47);
                                                                          pushToStrip(L46);
                                      pushToStrip(L25); pushToStrip(L34);
                                                                          pushToStrip(L44);
                                                                          pushToStrip(L43);
  pushToStrip( L2);                                                       pushToStrip(L42);
                    pushToStrip(L18); pushToStrip(L21); pushToStrip(L38);
}

void display40() {
                                                        pushToStrip(L31);
                                      pushToStrip(L27); pushToStrip(L32);
                    pushToStrip(L13);                   pushToStrip(L33);
  pushToStrip( L5);                                     pushToStrip(L34);
  pushToStrip( L4); pushToStrip(L15); pushToStrip(L24); pushToStrip(L35); pushToStrip(L44);
                                                        pushToStrip(L36);
                                                        pushToStrip(L37);
                                                        pushToStrip(L38);
}

void display50() {
  pushToStrip( L8); pushToStrip(L11); pushToStrip(L28); pushToStrip(L31); pushToStrip(L48);
  pushToStrip( L7);
  pushToStrip( L6);
  pushToStrip( L5); pushToStrip(L14); pushToStrip(L25); pushToStrip(L34);
                                                                          pushToStrip(L44);
                                                                          pushToStrip(L43);
  pushToStrip( L2);                                                       pushToStrip(L42);
                    pushToStrip(L18); pushToStrip(L21); pushToStrip(L38);
}

void display0() {
                    pushToStrip(L71); pushToStrip(L88); pushToStrip(L91);
  pushToStrip(L67);                                                       pushToStrip(L107);
  pushToStrip(L66);                                                       pushToStrip(L106);
  pushToStrip(L65);                                                       pushToStrip(L105);
  pushToStrip(L64);                                                       pushToStrip(L104);
  pushToStrip(L63);                                                       pushToStrip(L103);
  pushToStrip(L62);                                                       pushToStrip(L102);
                    pushToStrip(L78); pushToStrip(L81); pushToStrip(L98);
}

void display1() {
                                      pushToStrip(L88);
                    pushToStrip(L72); pushToStrip(L87);
  pushToStrip(L66);                   pushToStrip(L86);
                                      pushToStrip(L85);
                                      pushToStrip(L84);
                                      pushToStrip(L83);
                                      pushToStrip(L82);
  pushToStrip(L61); pushToStrip(L78); pushToStrip(L81); pushToStrip(L98); pushToStrip(L101);
}

void display2() {
                    pushToStrip(L71); pushToStrip(L88); pushToStrip(L91);
  pushToStrip(L67);                                                       pushToStrip(L107);
                                                                          pushToStrip(L106);
                                                                          pushToStrip(L105);
                                                        pushToStrip(L95);
                                      pushToStrip(L83);
                    pushToStrip(L77);
  pushToStrip(L61); pushToStrip(L78); pushToStrip(L81); pushToStrip(L98); pushToStrip(L101);
}

void display3() {
                    pushToStrip(L71); pushToStrip(L88); pushToStrip(L91);
  pushToStrip(L67);                                                       pushToStrip(L107);
                                                                          pushToStrip(L106);
                                      pushToStrip(L85); pushToStrip(L94);
                                                                          pushToStrip(L104);
                                                                          pushToStrip(L103);
  pushToStrip(L62);                                                       pushToStrip(L102);
                    pushToStrip(L78); pushToStrip(L81); pushToStrip(L98);
}

void display4() {
                                                        pushToStrip(L91);
                                      pushToStrip(L87); pushToStrip(L92);
                    pushToStrip(L73);                   pushToStrip(L93);
  pushToStrip(L65);                                     pushToStrip(L94);
  pushToStrip(L64); pushToStrip(L75); pushToStrip(L84); pushToStrip(L95); pushToStrip(L104);
                                                        pushToStrip(L96);
                                                        pushToStrip(L97);
                                                        pushToStrip(L98);
}

void display5() {
  pushToStrip(L68); pushToStrip(L71); pushToStrip(L88); pushToStrip(L91); pushToStrip(L108);
  pushToStrip(L67);
  pushToStrip(L66);
  pushToStrip(L65); pushToStrip(L74); pushToStrip(L85); pushToStrip(L94);
                                                                          pushToStrip(L104);
                                                                          pushToStrip(L103);
  pushToStrip(L62);                                                       pushToStrip(L102);
                    pushToStrip(L78); pushToStrip(L81); pushToStrip(L98);
}

void display6() {
                    pushToStrip(L71); pushToStrip(L88); pushToStrip(L91);
  pushToStrip(L67);                                                       pushToStrip(L107);
  pushToStrip(L66);
  pushToStrip(L65); pushToStrip(L74); pushToStrip(L85); pushToStrip(L94);
  pushToStrip(L64);                                                       pushToStrip(L104);
  pushToStrip(L63);                                                       pushToStrip(L103);
  pushToStrip(L62);                                                       pushToStrip(L102);
                    pushToStrip(L78); pushToStrip(L81); pushToStrip(L98);
}

void display7() {
  pushToStrip(L68); pushToStrip(L71); pushToStrip(L88); pushToStrip(L91); pushToStrip(L108);
                                                                          pushToStrip(L107);
                                                        pushToStrip(L93);
                                                        pushToStrip(L94);
                                      pushToStrip(L84);
                                      pushToStrip(L83);
                                      pushToStrip(L82);
                                      pushToStrip(L81);
}

void display8() {
                    pushToStrip(L71); pushToStrip(L88); pushToStrip(L91);
  pushToStrip(L67);                                                       pushToStrip(L107);
  pushToStrip(L66);                                                       pushToStrip(L106);
                    pushToStrip(L74); pushToStrip(L85); pushToStrip(L94);
  pushToStrip(L64);                                                       pushToStrip(L104);
  pushToStrip(L63);                                                       pushToStrip(L103);
  pushToStrip(L62);                                                       pushToStrip(L102);
                    pushToStrip(L78); pushToStrip(L81); pushToStrip(L98);
}

void display9() {
                    pushToStrip(L71); pushToStrip(L88); pushToStrip(L91);
  pushToStrip(L67);                                                       pushToStrip(L107);
  pushToStrip(L66);                                                       pushToStrip(L106);
  pushToStrip(L65);                                                       pushToStrip(L105);
                    pushToStrip(L75); pushToStrip(L84); pushToStrip(L95); pushToStrip(L104);
                                                                          pushToStrip(L103);
  pushToStrip(L62);                                                       pushToStrip(L102);
                    pushToStrip(L78); pushToStrip(L81); pushToStrip(L98);
}

///////////////////////
