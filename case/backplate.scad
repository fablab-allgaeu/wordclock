PlateWidth = 229;
StripWidth = 10;
StripLength = 166;
HSpacing = 156 / 10.0;
VSpacing = 1000 / 60.0;

difference() {
    square([PlateWidth, PlateWidth]);
    // mounting hole for power jack, 8mm
    translate([PlateWidth / 2.0, (PlateWidth - StripLength) / 4.0, 0]) circle(d=8);
    FixedHOffset = (PlateWidth - 11 * HSpacing) / 2.0 + (HSpacing - StripWidth) / 2.0;
    for (x = [0:10]) {
        translate([FixedHOffset + x * HSpacing, (PlateWidth - StripLength) / 2.0, 0]) square([StripWidth, StripLength]);
    }
}