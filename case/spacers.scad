LedSpacingH=174.0 / 11; // idk if the LED spacing is correct
LedSpacingV=17.4;       // should be deduce
MaterialThickness=4;

CaseDepth=38-4-4-7;  // change this to actual dimensions
CaseWidth=230; // change this to actual dimensions
FaceWidth=170; // change this to actual dimensions

// normal spacers for light cassette
module VSpacer() {
    difference() {
        square([FaceWidth + 2 * MaterialThickness, CaseDepth]);
        for(i = [0:10]) {
            translate([i * LedSpacingV, CaseDepth/2.0, 0]) square([MaterialThickness, CaseDepth/2.0]);
        }
    }
}

module HSpacer() {
    difference() {
        square([FaceWidth + 2 * MaterialThickness, CaseDepth]);
        for(i = [0:11]) {
            translate([i * LedSpacingH, CaseDepth/2.0, 0]) square([MaterialThickness, CaseDepth/2.0]);
        }
    }
}

// long spacers to hold the light cassette in place (go on the outmost positions)
module VSpacerLong() {
    difference() {
        square([CaseWidth, CaseDepth]);
        translate([(CaseWidth - FaceWidth) / 2.0 - MaterialThickness, 0, 0]) {
            for(i = [0:10]) {
                translate([i * LedSpacingV, CaseDepth/2.0, 0]) square([MaterialThickness, CaseDepth/2.0]);
            }
            translate([0, CaseDepth - 2, 0]) square([FaceWidth + MaterialThickness, 2]);
        }
    }
}

module HSpacerLong() {
    difference() {
        square([CaseWidth, CaseDepth]);
        translate([(CaseWidth - FaceWidth) / 2.0 - MaterialThickness, 0, 0]) {
            for(i = [0:11]) {
                translate([i * LedSpacingH, CaseDepth/2.0, 0]) square([MaterialThickness, CaseDepth/2.0]);
            }
            translate([0, CaseDepth - 2, 0]) square([FaceWidth + MaterialThickness, 2]);
        }
    }
}

// 9 horizontal spacers
for(i = [0:8]) {
    translate([180, (CaseDepth + 1) * i, 0]) HSpacer();
}
// 10 vertical spacers
for(j = [0:9]) {
    translate([0, j * (CaseDepth + 1), 0]) VSpacer();
}
// and two each of the longer versions
for(k = [0:1]) {
    translate([0, 10 * (CaseDepth + 1) + k * (CaseDepth + 1), 0]) VSpacerLong();
    translate([0, 12 * (CaseDepth + 1) + k * (CaseDepth + 1), 0]) HSpacerLong();
}