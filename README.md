# Projekt DIY Wordclock

## Beschreibung
Wordclocks oder Wortuhren erfreuen sich seit einigen Jahren wachsender
Beliebtheit bei Bastlern und Raumgestaltern. Sie sind ein Hingucker in jedem
Regal oder an der Wand und sind dabei doch mit vergleichsweise wenig Aufwand
herzustellen. Wordclocks sind Uhren, die anstatt von Zeigern oder Ziffern
Wörter zur Anzeige der Zeit verwenden. In einem Gitter ähnlich einem
Kreuzworträtsel finden sich die passenden Wörter und Satzteile, die benötigt
werden um Angaben wie etwa „ES IST ZEHN NACH SEHS“ zu machen.    

![Frontplatte der Wordclock](./wordclock.png "Frontplatte der Wordclock")

## Zielgruppe
Der Workshop ist an erwachsene Personen mit Bastelerfahrung (nicht zwangsweise
Elektronik-/Lötkenntnissen) gerichtet.

## Teilnehmeranzahl
Die teilnehmeranzahl lässt sich auch sehr variabel gestalten. Am besten sind 5
bis 10 Teilnehmer.

## Ablauf des Workshops
  - Begrüßung und Einführung ins Lab, Erklärung zur Wordclock _(15min)_
  - Ausgabe der Matierialien und Werkzeuge _(15min)_
  - Aufbau der Cassette und gestaltung der Frontplatte _(30h)_
  - Einführung ins Löten und Aufbau der LED Matrix und Steuerung _(3h)_
  - Finaler Zusammenbau der Uhr _(30min)_

Die genauen Zeiten sollten in einem Probelauf getestet werden.

## Stückliste pro Teilnehmer
Jeder Teilnehmer benötigt folgende Komponenten

| Name         | Beschreibung                  | Quelle      | Anzahl  |  Preis
| :----------- | :---------------------------- | :---------- | ------: | -----:
| Steuerung    | Arduino Nano                  | Amazon      |   1     |     3€
| RTC          |                               | Ebay        |   1     |     3€
| RIBBA        | Bilderrahmen von IKEA         | IKEA/Amazon |   1     |     5€
| Cassette     | Gefache aus dem Lasercutter   | Kempodium   |   1     |     6€
|              | aus 4mm Pappelholz            |             |         |
| Frontplatte  | kann aus beliebigem Material  | -/-         |   1     |    -/-
|              | geschnitten sein              |             |         |    15€
| Netzteil     | 5V Schaltnetzteil, 1,2 A      | Pollin      |   1     |     3€
| Strombuchse  | Hohlsteckerbuchse 5,5 / 2,1   | Pollin      |   1     |     1€
| WS2812b LEDs | einfach anzusteuern, sehr     | Amazon      |   110   |    12€
|              | hell, 5m mit 300 LEDs         |             |         |
| Knöpfe       |                               |             |   3     |     3€
| LDR          |                               |             |   1     |     1€
| Litze        | Verkabelung                   |             |   -     |     1€
| GESAMT       |                               |             |         |    60€

## Raum- und Werkstattnutzung
Wenn alle Teilnehmer ins Labor passen, wird nur dieser Raum und der
Maschinenraum benötigt, ansonsten auch noch der Raum gegenüber. Die
Holzwerkstatt wird nicht unbedingt gebraucht. Denkbar wäre auch die Nutzung der
Kinderwerkstatt statt des Labors.

## Betreuer
Am besten zwei Betreuer, einer ist auch denkbar. Die Betreuer müssen löten und
die Schaltung verstehen können. Verständnis des Codes für die Firmware ist für
Anpassungen (z.B. "viertel vor" / "dreiviertel") wichtig.

## Werkzeug- und Maschinennutzung
Für jeden einzelnen Teilnehmer:
  - Lötkolben
  - Seitenschneider
  - Cuttermesser
  - Lötzinn
  - Klebeband

Maschinen:
  - Lasercutter
  - Akkuschrauber
  - 5mm Bohrer
  - 1mm Bohrer
