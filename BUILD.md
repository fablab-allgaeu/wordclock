# Aufbauanleitung Wordclock

## Stückliste
| Anzahl | Bezeichnung
| -----: | :---------------------------
|      1 | Arduino Nano
|      1 | DS3231 RTC Modul
|      1 | Elektrolytkondensator
|      1 | Fotowiderstand
|      1 | 150Ω Widerstand
|    110 | WS2812b LEDs auf Trägerstreifen
|      1 | Holbuchsenstecker
|      1 | Netzteil
|      1 | RIBBA Bilderrahmen
|      1 | Ziffernblatt
|      1 | Deckelplatte
|      1 | Diffusionspapier
|      9 | Abstandsplatten mit 12 Schlitzen, kurz
|     10 | Abstandsplatten mit 11 Schlitzen, kurz
|      2 | Abstandsplatten mit 12 Schlitzen, lang
|      2 | Abstandsplatten mit 11 Schlitzen, lang
|        | Schaltlitzen
|        | Lötkolben
|        | Lötzinn
|        | Heißklebepistole
|        | Holzleim

## Gehäuse
### Lichtkassette
Die Lichtkassette schirmt die einzelnen Buchstaben vor dem Licht der
angrenzenden LEDs ab, sodass jede LED nur genau einen Buchstaben beleuchtet. Sie
ist aus eizelnen Stegen aufgebaut, die ineinandergesteckt ein Gitter ergeben.
Die äußersten Stege sind länger und dienen der Positionierung der Kassette im
Bilderrahmen.   
Begonnen wird mit den beiden langen Platten mit 12 Schlitzen. Darauf werden die
beiden langen Platten mit 11 Schlitzen in die äußersten Schlitze gesteckt. Die
verbleibenden 10 Schlitze werden mit den 10 kürzeren Platten mit 11 Schlitzen
gefüllt. Das Gerüst wird vorsichtig umgedreht und die verbleibenden 9 Platten in
die restlichen Schlitze gesteckt.   
Optional kann das Gitter je nach verwendetem Material mit dem Holzleim oder
Bastelkleber verklebt werden. Es ist allerdings darauf zu achten, dass das Gitter
rechtwinklig und flach zusammengeleimt wird. Bei den vom FabLab ausgegebenen
Kassetten aus Pappe sollte es jedoch reichen, die Stecke zusammenzustecken.

### Deckel
Der Deckel ist die mit Linien versehene Holzplatte mit Loch. Auf diese werden
die LEDs geklebt. Vorher muss der lange LED Streifen in kleinere Streifen à 10
LEDs geteilt werden. Dies kann mit einer Schere oder einem Seitenschneider
erfolgen. Eventuell befinden sich an den zu schneidenden Stellen Lötstellen.
Diese können nich zerschnitten werden und müssen entlötet werden. Dazu schneidet
man vorsichtig das Klebeband auf der Rückseite mit einem Messer ein und erhitzt
dann alle drei Lötstellen gleichzeitig mit dem Lötkolben bis das Zinn flüssig
wird und der Streifen sich trennt.   
Die 11 Streifen werden nun anhand der Linien auf dem Deckel ausgerichtet und
aufgeklebt. **Man muss hierbei die Richtung der Datenleitungen beachten!** Auf
einen _DO_ Pin muss ein mit _DI_ markierter Pin folgen; die Streifen müssen in
Schlangenlinien aufgeklebt werden.   
Danach kann die Buchse zur Stromversorgung in das dafür vorgesehene Loch
geschraubt werden. Die Lötanschlüsse müssen auf der Seite der LEDs sein.   

### Fotowiderstand
Der Fotowiderstand dient dem Messen der Umgebungshelligkeit und muss am Gehäuse
befestigt werden. Die sauberste Methode hierfür ist, den Widerstand in die
Oberseite des Bilderrahmens einzulassen. Hierfür ist ein 6mm großes Loch etwa
1-2mm tief in die Oberseite des Rahmens zu bohren. Zur Durchführung der Drähte
müssen zudem vorsichtig zwei 1mm große Löcher am Rand der Vertiefung gebohrt
werden. Der Abstand entspricht dem Abstand der Drähte am Fotowiderstand.   
Sind die Löcher gebohrt, werden die Drähte durch das Gehäuse geführt und der
Widerstand in die Vertiefung gedrückt. Eventuell kann er festgeklebt werden, damit
er nicht herausfallen kann.   

## Elektronik
### Arduino Nano
Der Arduino wird mittels USB Kabel am PC angeschlossen und mit der Firmware
bespielt. Dazu öffnet man die `firmware/firmware.ino` Datei mit der Arduino
Software, Wählt unter _Werkzeuge_ das Board _Arduino Nano ATmega328_ aus und
als _Port_ den Port an dem der Arduino angeschlossen ist. Anschließend drückt
man auf den mit einem Pfeil markierten Knopf _Hochladen_ in der Werkzeugleiste.
Die Firmware wird nun auf den Mikrocontroller übertragen und man kann das Board
wieder abstecken, nachdem über dem Ausgabefenster _Hochladen abgeschlossen_
erscheint.

### DS3231 RTC Modul
Das RTC Modul muss mit Strom verbunden und mit den Datenleitungen des Arduino
verbunden werden. _VCC_ (5v) und _GND_ (0V) werden durch eine rote und eine schwarze
Leitung mit den entsprechneden Pins am Arduino verbunden. _SDA_ wird über ein grünes
Kabel mit _Pin A4_ am Arduino verbunden, _SCL_ über ein gelbes mit _A5_. Der mit
_SQ_, _SQR_, _INT_ oder ähnlichem beschriftete Pin wird mit _Pin 2_ des Arduino
verbunden.   

### LED Matrix
Wie bereits angedeutet werden die LEDs alle hintereinander geschalten. Die
einfachste Methode hierfür ist das Verkabeln in Schlangenlinienform. Alle _GND_
und _VCC_ Pins des vorigen Streifens müssen mit den _GND_ und _VCC_ Pins des
nächsten Streifens verlötet. Außerdem wird jeder _DO_ Pin mit dem nächsten _DI_
Pin verbunden.   
Hierfür kann man sich entsprechende Drahtstücke zurechtschneiden, entisolieren
und vorverzinnen. Zur besseren Übersicht kann sollte man für _GND_ Verbindungen
schwarze, für _VCC_ Verbindungen rote und für die Datenverbindungen etwa grüne
oder gelbe Kabel verwenden. Dies ist jedoch nicht zwingend notwendig.   
Sind alle LED Streifen miteinander verbunden, werden noch die _GND_ und _VCC_
Pins des ersten und letzten Streifens durch längere Kabel verbunden und an den
Eingängen des ersten Streifens längere Drähte zur Verbindung mit dem
Mikocontroller angebracht.   

### Taster
Die beiden Taster für MENU und PLUS werden auf je einer Seite mit _GND_ verbunden.
Die andere Seite des MENU Tasters wird an _Pin 3_ am Arduino, PLUS an _Pin 4_
angeschlossen.

### Fotowiderstand
Der Fotowiderstand bildet zusammen mit dem 150Ω Widerstand einen Spannungsteiler,
dessen Spannung mit _Pin A0_ des Arduino gemessen wird. Der 150Ω Widerstand wird
zwischen _GND_ und _Pin A0_ verlötet, der Fotowiderstand über kabel mit _Pin A0_
und _VCC_ verbunden.
